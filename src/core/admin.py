from django.contrib import admin
from core.models import Historial_entrega, Usuario, Trabajador, Solicitudes, Documento, Audio


admin.site.register(Usuario)
admin.site.register(Trabajador)
admin.site.register(Solicitudes)
admin.site.register(Documento)
admin.site.register(Historial_entrega)
admin.site.register(Audio)

from django.shortcuts import render, redirect,get_object_or_404
from .forms import UsuarioForms
from .forms import TrabajadorForms
from .forms import SolicitudesForms
from .forms import adocumentoForms
from .forms import AudioForms
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse
from .models import Documento, Solicitudes, Usuario
from .models import Audio



def index(request):
    template_name = 'index.html'
    context = {}
    usuarioslistados = Usuario.objects.all().order_by('name')
    print(usuarioslistados)
    context ['Usuarioc'] = usuarioslistados
    return render(request, template_name, context)

def lsolicitudes(request):
    template_name = 'lsolicitudes.html'
    context = {}
    solicitudlistados = Solicitudes.objects.all().filter(estado='P')
    print(solicitudlistados)
    context ['soliform'] = solicitudlistados
    return render(request, template_name, context)

def lsolicitudesen(request):
    template_name = 'lsolicitudesen.html'
    context = {}
    solicitudlistadosE = Solicitudes.objects.all().filter(estado='E')
    print(solicitudlistadosE)
    context ['soliformE'] = solicitudlistadosE
    return render(request, template_name, context)

def editarsoli(request, id):
    template_name = 'editarsoli.html'
    context = {}
    solicitudes = get_object_or_404(Solicitudes, id=id)
    context['formsoli'] = SolicitudesForms(instance=solicitudes)
    if request.POST:
        form_soli = SolicitudesForms(request.POST, instance=solicitudes, files=request.FILES)
        if form_soli.is_valid():
            form_soli.save()
            return HttpResponseRedirect(reverse('lsolicitudes'))
    return render(request, template_name, context)

def resgis_us(request):
    template_name = '_resgis_us.html'
    context = {}
    if request.POST:
        form_usuario = UsuarioForms(request.POST)
        if form_usuario.is_valid():
            form_usuario.save()
            return HttpResponseRedirect(reverse('index'))
    context['form'] = UsuarioForms()
    
    return render(request, template_name, context)

def resgis_tra(request):
    template_name = '_resgis_tra.html'
    context = {}
    if request.POST:
        form_trabajador = TrabajadorForms(request.POST)
        if form_trabajador.is_valid():
            form_trabajador.save()
            return HttpResponseRedirect(reverse('login'))
    context['formulario'] = TrabajadorForms()
    
    return render(request, template_name, context)
    
def login(request):
    template_name = '_login.html'
    context = {}
    
    return render(request, template_name, context)

def editarusu(request, id):
    template_name = 'editarusu.html'
    context = {}
    usuario = get_object_or_404(Usuario, id=id)
    context['form'] = UsuarioForms(instance=usuario)
    if request.POST:
        form_usuario = UsuarioForms(request.POST, instance=usuario, files=request.FILES)
        if form_usuario.is_valid():
            form_usuario.save()
            return HttpResponseRedirect(reverse('index'))
    return render(request, template_name, context)

def editartra(request):
    template_name = 'editartra.html'
    context = {}
    
    return render(request, template_name, context)

def usuario(request):
    template_name = '_usuario.html'
    context = {}

    return render(request, template_name, context)

def eliminar_usuario(request):
    print('llegue')
    if request.POST:
        print('post')
        usuario = Usuario.objects.get(id=int(request.POST['info']))
        usuario.delete()

    return JsonResponse({'state':'ok'})

def eliminar_solicitud(request):
    if request.POST:
        print('post')
        solicitudes = Solicitudes.objects.get(id=int(request.POST['info']))
        solicitudes.delete()
    return JsonResponse({'state':'ok'})

def Csolicitud(request):
    template_name = 'Csolicitud.html'
    context = {}
    if request.POST:
        form_soli = SolicitudesForms(request.POST)
        if form_soli.is_valid():
            form_soli.save()
            return HttpResponseRedirect(reverse('lsolicitudes'))
    context['formsoli'] = SolicitudesForms()

    return render(request, template_name, context)

def ldocumentos(request):
    template_name = 'ldocumentos.html'
    context = {}
    documentoslistados = Documento.objects.all().order_by('n_documento')
    context ['Documentol'] = documentoslistados

    return render(request, template_name, context)

def adocumento(request):
    template_name = 'adocumento.html'
    context = {}
    if request.POST:
        form_adocumento = adocumentoForms(request.POST,request.FILES)
        if form_adocumento.is_valid():
            doc = form_adocumento.save()
            doc.generate_qr()


            return HttpResponseRedirect(reverse('ldocumentos'))
    context['docuform'] = adocumentoForms()
    return render(request, template_name, context)

def agaudio(request):
    template_name = 'agaudio.html'
    context = {}
    if request.POST:
        form_laudio = AudioForms(request.POST,request.FILES)
        if form_laudio.is_valid():
            form_laudio.save()
            return HttpResponseRedirect(reverse('agaudio'))
    context['agaudioform'] = AudioForms()
    return render(request, template_name, context)

def laudio(request):
    template_name = 'laudio.html'
    context = {}
    audioslistados = Audio.objects.all().order_by('n_docu')
    print(audioslistados)
    context ['laudio'] = audioslistados
    return render(request, template_name, context)
# Generated by Django 3.1.4 on 2021-06-29 03:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_remove_trabajador_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solicitudes',
            old_name='Estado',
            new_name='estado',
        ),
        migrations.RenameField(
            model_name='solicitudes',
            old_name='Hora',
            new_name='hora',
        ),
        migrations.RemoveField(
            model_name='documento',
            name='Tipo',
        ),
        migrations.RemoveField(
            model_name='documento',
            name='estado',
        ),
        migrations.AddField(
            model_name='documento',
            name='media',
            field=models.FileField(null=True, upload_to='media/'),
        ),
        migrations.AddField(
            model_name='solicitudes',
            name='usuario',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.usuario'),
        ),
    ]

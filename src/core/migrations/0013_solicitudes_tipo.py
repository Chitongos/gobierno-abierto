# Generated by Django 3.1.4 on 2021-07-22 04:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_documento_qr'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudes',
            name='tipo',
            field=models.CharField(choices=[('A', 'Audio'), ('T', 'Texto')], max_length=1, null=True, verbose_name='Tipo'),
        ),
    ]

from os import truncate
from django.db import models
from django.contrib.auth.models import User as dj_user

class Usuario(models.Model):
    GENRE = (
        ('M', 'Masculino'),
        ('F', 'Femenino')
    )
    name = models.CharField('Nombre', max_length=50)
    apellido = models.CharField('Apellido', max_length=50)
    rut = models.CharField('Rut', max_length=10)
    direccion = models.CharField('Direccion', max_length=50)
    genero = models.CharField('genero', max_length=1, choices=GENRE)
    edad = models.PositiveIntegerField('Edad')

    def __str__(self) -> str:
        return self.rut

class Trabajador(models.Model):
    nombre = models.CharField('nombre', max_length=50)
    apellido = models.CharField('apellido', max_length=50)
    rut = models.CharField('rut', max_length=10)
    area_trabajo = models.CharField('area_trabajo', max_length=20)

class Documento(models.Model):
    name = models.CharField('Nombre', max_length= 50)
    n_documento = models.CharField('n_documento', max_length=20)
    media = models.FileField(upload_to='media/', null=True)
    qr = models.TextField(default="")

    def generate_qr(self):
        import qrcode, base64
        from io import BytesIO

        qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=4,
        border=4,
        )

        qr.add_data(self.media.url)
        qr.make(fit=True)
        img = qr.make_image()

        buffered = BytesIO()
        img.save(buffered, format="PNG")
        self.qr = base64.b64encode(buffered.getvalue()).decode("utf-8")
        self.save()


    def __str__(self) -> str:
        return self.n_documento
 
class Jefe(models.Model):
    name = models.CharField('Nombre', max_length=50)
    apellido = models.CharField('Apellido',max_length=50)
    Cargo = models.CharField('Cargo', max_length=20)

class Historial_entrega(models.Model):
    Fecha = models.CharField('Fecha', max_length=10)
    Estado = models.CharField('Estado', max_length=20)
    n_documento = models.CharField('n_documento', max_length=10)


class Audio(models.Model):
    name = models.CharField('Nombre', max_length= 50)
    audio = models.FileField(upload_to='audio/', null= True)
    n_docu = models.ForeignKey(Documento, null=True, on_delete=models.SET_NULL)
    qr = models.TextField(default="")

    def generate_qr(self):
        import qrcode, base64
        from io import BytesIO

        qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=4,
        border=4,
        )

        qr.add_data(self.media.url)
        qr.make(fit=True)
        img = qr.make_image()

        buffered = BytesIO()
        img.save(buffered, format="PNG")
        self.qr = base64.b64encode(buffered.getvalue()).decode("utf-8")
        self.save()

    def __str__(self) -> str:
        return self.name

class Solicitudes(models.Model):
    ESTAD=(
        ('E','Entregado'),
        ('P','Pendiente')
    )
    TIP=(
        ('A','Audio'),
        ('T','Texto')
    )
    ENT=(
        ('Q','Código QR'),
        ('E','Correo electronico')
    )
    fecha = models.DateTimeField(auto_now=False,null=True)
    titulo = models.CharField('Titulo', max_length=50, null=True)
    estado = models.CharField('Estado', max_length=1,choices=ESTAD)
    descripcion = models.CharField('descripcion', max_length = 250, default="")
    usuario = models.ForeignKey(Usuario,null=True,on_delete=models.SET_NULL)
    Enl_audio = models.ForeignKey(Audio, null=True, on_delete=models.SET_NULL)
    tipo = models.CharField('Tipo', max_length=1,choices=TIP,null=True)
    entrega = models.CharField('Entrada',max_length=1,choices=ENT,null=True)

  
from django import forms
from django.forms import fields
from .models import Usuario
from .models import Trabajador
from .models import Solicitudes
from .models import Documento
from .models import Audio

class UsuarioForms(forms.ModelForm):
    class Meta: 
        model = Usuario
        fields = ['name', 'apellido', 'rut', 'direccion', 'genero', 'edad']

class TrabajadorForms(forms.ModelForm):
    class Meta:
        model = Trabajador
        fields = ['nombre', 'apellido', 'rut', 'area_trabajo']

class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'

class SolicitudesForms(forms.ModelForm):
    class Meta:
        model = Solicitudes
        fields = ['fecha', 'estado', 'descripcion','usuario','titulo', 'Enl_audio','tipo','entrega']
        widgets = {
            
            'fecha': DateTimeInput(attrs={'class': 'form-control'}),
            
        }   
class adocumentoForms(forms.ModelForm):
    class Meta:
        model = Documento
        fields = ['name', 'n_documento', 'media']

class AudioForms(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ['name', 'audio', 'n_docu']
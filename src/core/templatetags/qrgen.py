from django import template
register = template.Library()

@register.simple_tag
def generate_qr(request, *args,**kwargs):
    import qrcode, base64
    from io import BytesIO

    qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=4,
    border=4,
    )
    url = ""
    if request.is_secure():
        url = 'https://'+ request.get_host()+kwargs['media_url']

    else:
        url = 'http://'+ request.get_host()+kwargs['media_url']

    qr.add_data(url)
    qr.make(fit=True)
    img = qr.make_image()

    buffered = BytesIO()
    img.save(buffered, format="PNG")
    
    return 'data:image/png;base64,'+base64.b64encode(buffered.getvalue()).decode("utf-8")
    
from os import name
from django.urls import path
from core import views

urlpatterns = [
    path('', views.index, name='index'),
    path('lsolicitudes/', views.lsolicitudes, name='lsolicitudes'),
    path('registrar/usuario/', views.resgis_us, name='resgis_us'),
    path('registrar/trabajador/', views.resgis_tra, name='resgis_tra'),
    path('login/', views.login, name='login'),
    path('editarusu/<id>', views.editarusu, name='editarusu'),
    path('Csolicitud/', views.Csolicitud, name='Csolicitud'),
    path('editartra/', views.editartra, name='editartra'),
    path('usuario/', views.usuario, name='usuario'),
    path('eliminarUsuario/', views.eliminar_usuario, name='eliminarUsuario'),
    path('ldocumentos/', views.ldocumentos, name='ldocumentos'),
    path('adocumento/',views.adocumento, name='adocumento'),
    path('editarsoli/<id>', views.editarsoli, name='editarsoli'),
    path('eliminarSolicitud/', views.eliminar_solicitud, name='eliminarSolicitud'),
    path('lsolicitudesen/', views.lsolicitudesen, name='lsolicitudesen'),
    path('laudio/', views.laudio, name='laudio'),
    path('agaudio/', views.agaudio, name='agaudio')

    
]

